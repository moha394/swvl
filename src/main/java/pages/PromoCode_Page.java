package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PromoCode_Page extends Base_Page {
    public PromoCode_Page(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }
    @FindBy(id="io.swvl.customer:id/promo_code_edit_text")
    WebElement EditText;

    @FindBy(id="io.swvl.customer:id/apply_promo_code")
    WebElement ApplyBtn;

    String Code = "Cai30";
    public void AddCode(){

        EditText.sendKeys(Code);
        F_wait(By.id("io.swvl.customer:id/promo_code_edit_text"));
        ApplyBtn.click();
        F_wait(By.id("io.swvl.customer:id/apply_promo_code"));

    }


}
