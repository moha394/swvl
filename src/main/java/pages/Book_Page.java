package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class Book_Page extends Base_Page{
    public Book_Page(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }


    @FindBy(id="io.swvl.customer:id/book_btn")
    WebElement Book_btn;

    @FindBy(id="io.swvl.customer:id/done_btn")
    WebElement Done_btn;

    @FindBy(className = "android.widget.TextView")
    List<WebElement> li;


    public void ClickOnPromo(){
        getElementByText(li,"ADD PROMO CODE").click();}
    public void Book(){
        Book_btn.click();
        F_wait(By.id("io.swvl.customer:id/book_btn"));
    }
    public void Done(){
        Done_btn.click();
        F_wait(By.id("io.swvl.customer:id/done_btn"));
    }
}
