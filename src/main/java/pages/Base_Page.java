package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class Base_Page {
    protected AndroidDriver<AndroidElement> driver;
    public Base_Page(AndroidDriver<AndroidElement>driver){
        PageFactory.initElements(driver,this);
    }

    public WebElement getElementByText(List<WebElement>li, String text){
        WebElement element = null;

        for (WebElement webElement1 :li)
        {
            if(webElement1.getAttribute("text").contains(text)){
                element = webElement1;
            }
        }
    
    
    return element;
    }

    public int CountErrors(List<WebElement> li , String text){
    int count =0;
        for (WebElement webElement : li){
            if(webElement.getAttribute("text").contains(text)){
                count++;
            }
        }
        return  count;
    }


    public void F_wait(By by){
        try{
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(NullPointerException.class);
        WebElement message = wait
                .until(driver -> driver.findElement(by));
    }
        catch (Exception ex){

        }
    }



}
