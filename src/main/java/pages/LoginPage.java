package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LoginPage extends Base_Page {
    public LoginPage(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }


    @FindBy(id = "io.swvl.customer:id/google_tv")
    WebElement GoogleBtn;

    @FindBy(id = "io.swvl.customer:id/facebook_tv")
    WebElement FacebookBtn;


    @FindBy(id = "/io.swvl.customer:id/hint")
    WebElement PhoneBtn;

    @FindBy(id="com.google.android.gms:id/add_account_chip_title")
    WebElement Another;

    @FindBy(className = "android.widget.TextView")
    List<WebElement> li;




   // String Email = "moha2014394@gmail.com";
    public void ClickOnGoogle(){GoogleBtn.click();}
    public void LoginWithGmailAccount(String Email) throws InterruptedException {
        if(Another.isDisplayed()){
            getElementByText(li,Email).click();

        }
        else {
            F_wait(By.id("com.google.android.gms:id/add_account_chip_title"));
            getElementByText(li,Email).click();

        }



    }

}
