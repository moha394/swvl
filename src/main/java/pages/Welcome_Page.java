package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class Welcome_Page extends Base_Page {
    public Welcome_Page(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    @FindBy(id="com.android.packageinstaller:id/permission_allow_button")
    WebElement allowBtn;
    @FindBy(id="io.swvl.customer:id/skip_btn")
    WebElement SkipBtn;

    @FindBy(id="io.swvl.customer:id/continue_btn")
    WebElement ContBtn;
    @FindBy(className = "android.widget.Button")
    List<WebElement> li;

    public void Allow(){
        getElementByText(li,"Allow").click();
    }

    public void Skip() throws InterruptedException {
        SkipBtn.click();
        F_wait(By.id("io.swvl.customer:id/skip_btn"));
    }

}
