package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class Where_Page extends  Base_Page {
    public Where_Page(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    @FindBy(id="io.swvl.customer:id/where_to_coach_marks")
    WebElement CoachWhereBtn;


    @FindBy(id="io.swvl.customer:id/dropoff_et")
    WebElement DropBtn;

    @FindBy(id="io.swvl.customer:id/places_rv")
    WebElement SuggestionField;

    @FindBy(className = "android.widget.TextView")
    List<WebElement> li;



    //String DropName = "Concord Plaza Mall";



    public void SelectCoach(){

        CoachWhereBtn.click();
        F_wait(By.id("io.swvl.customer:id/where_to_coach_marks"));
    }

    public void SelectDropField(){
        DropBtn.click();
        F_wait(By.id("io.swvl.customer:id/dropoff_et"));

    }

    public void FillDrop(String DropName)

    {
        DropBtn.sendKeys(DropName);
    }

    public void SelectDropPoint(String DropName){

            if(SuggestionField.isDisplayed()){
                getElementByText(li,DropName).click();

            }
            else{
                F_wait(By.id("io.swvl.customer:id/places_rv"));
                getElementByText(li,DropName).click();
            }


    }

}
