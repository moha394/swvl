package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Map_Page extends Base_Page {
    public Map_Page(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    @FindBy(id="io.swvl.customer:id/next_btn")
    WebElement Next_btn;

    public void Next(){

        Next_btn.click();
        F_wait(By.id("io.swvl.customer:id/next_btn"));
    }
}
