package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class Trips_Page extends Base_Page{
    public Trips_Page(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    @FindBy(className = "android.widget.TextView")
    List<WebElement> li;

    @FindBy(id="io.swvl.customer:id/date_btn")
    WebElement DateBtn;

    @FindBy(className = "android.widget.Button")
    List<WebElement> li2;

    //String _Time = "08:00 a.m.";
    public void SelectByTime(String _Time){

        if(DateBtn.isDisplayed())  getElementByText(li,_Time).click();
        else
            {
                F_wait(By.id("io.swvl.customer:id/date_btn"));
            }
    }

    public void SelectByDate(String _Date){

        if(DateBtn.isDisplayed())  getElementByText(li2,_Date).click();
        else
        {
            F_wait(By.id("io.swvl.customer:id/date_btn"));
        }
    }




}
