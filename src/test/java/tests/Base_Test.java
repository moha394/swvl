package tests;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Base_Test {
    static AndroidDriver<AndroidElement> driver;

    @BeforeSuite
    public static void SetCapabilities() throws MalformedURLException {
        File app = new File(System.getProperty("user.dir")+"\\APK\\Swvl.apk");

        DesiredCapabilities caps =new DesiredCapabilities();
        caps.setCapability("platformName", "Android");
        caps.setCapability("deviceName", "TestDevice");
        caps.setCapability("app", app.getAbsolutePath());
        caps.setCapability("autoGrantPermissions", "true");
        caps.setCapability("appPackage", "io.swvl.customer");
        driver = new AndroidDriver<AndroidElement>(new URL("http://localhost:4723/wd/hub"), caps);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);


    }

    @AfterSuite (description = "Generate Report")
    public static void close() throws InterruptedException, IOException {

        //Open Allure Reports
        Runtime.getRuntime().exec("powershell.exe allure serve allure-results");

        Thread.sleep(3000);
        driver.quit();
    }
}
