package tests;

import data.Excel_Reader;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.*;

import java.io.IOException;

public class LoginTest extends Base_Test {

    Welcome_Page WelcomePageObject;
    LoginPage LoginPageObject;
    Where_Page WherePageObject;
    Map_Page MapPageObject;
    Trips_Page TripsPageObject;
    Book_Page BookPageObject;
    PromoCode_Page PromoPageObject;

    //Data Driven Test
    @DataProvider(name="Data")
    public Object [][] Data() throws IOException {
        Excel_Reader ER = new Excel_Reader();
        return ER.getExcelData();
    }

    @Test(description = "HappyScenario",dataProvider="Data")
    void BookRide(String Email ,String DropName , String time,String date) throws InterruptedException {

        //Skip To login Page
       // WelcomePageObject = new Welcome_Page(driver);
       // WelcomePageObject.Skip();

        // Login Page
        LoginPageObject = new LoginPage(driver);
        LoginPageObject.ClickOnGoogle();
        LoginPageObject.LoginWithGmailAccount(Email);

        //Where To page
        WherePageObject = new Where_Page(driver);
        WherePageObject.SelectCoach();
        WherePageObject.SelectDropField();
        WherePageObject.FillDrop(DropName);
        WherePageObject.SelectDropPoint(DropName);

        // Selecting Trips
        TripsPageObject = new Trips_Page(driver);
        TripsPageObject.SelectByDate(date);
        TripsPageObject.SelectByTime(time);

        // Next to Book Page
        MapPageObject = new Map_Page(driver);
        MapPageObject.Next();

        //Book a Trip Page
        BookPageObject = new Book_Page(driver);
        BookPageObject.Book();
        BookPageObject.Done();

    }


}
